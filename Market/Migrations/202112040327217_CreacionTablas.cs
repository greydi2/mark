﻿namespace Market.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreacionTablas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClasificacionProductos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Codigo = c.String(maxLength: 15),
                        Clasificacion = c.String(nullable: false, maxLength: 75),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Codigo, unique: true);
            
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrdenDetalles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrdenId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        Precio = c.Double(nullable: false),
                        Cantidad = c.Int(nullable: false),
                        Descuento = c.Int(nullable: false),
                        Total = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ordenes", t => t.OrdenId)
                .ForeignKey("dbo.Productos", t => t.ProductoId)
                .Index(t => t.OrdenId)
                .Index(t => t.ProductoId);
            
            CreateTable(
                "dbo.Ordenes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FechaCreacion = c.DateTime(nullable: false),
                        ClientesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clientes", t => t.ClientesId)
                .Index(t => t.ClientesId);
            
            CreateTable(
                "dbo.Productos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Codigo = c.String(maxLength: 15),
                        Nombre = c.String(nullable: false, maxLength: 75),
                        Estado = c.Boolean(nullable: false),
                        FechaCreacion = c.DateTime(nullable: false),
                        UnidadMedidaId = c.Int(nullable: false),
                        ClasificacionProductoId = c.Int(nullable: false),
                        Precio = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClasificacionProductos", t => t.ClasificacionProductoId)
                .ForeignKey("dbo.UnidadMedidas", t => t.UnidadMedidaId)
                .Index(t => t.Codigo, unique: true)
                .Index(t => t.UnidadMedidaId)
                .Index(t => t.ClasificacionProductoId);
            
            CreateTable(
                "dbo.UnidadMedidas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Codigo = c.String(maxLength: 15),
                        Descripcion = c.String(nullable: false, maxLength: 255),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Codigo, unique: true);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Usuario = c.String(nullable: false, maxLength: 50),
                        Clave = c.String(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrdenDetalles", "ProductoId", "dbo.Productos");
            DropForeignKey("dbo.Productos", "UnidadMedidaId", "dbo.UnidadMedidas");
            DropForeignKey("dbo.Productos", "ClasificacionProductoId", "dbo.ClasificacionProductos");
            DropForeignKey("dbo.OrdenDetalles", "OrdenId", "dbo.Ordenes");
            DropForeignKey("dbo.Ordenes", "ClientesId", "dbo.Clientes");
            DropIndex("dbo.UnidadMedidas", new[] { "Codigo" });
            DropIndex("dbo.Productos", new[] { "ClasificacionProductoId" });
            DropIndex("dbo.Productos", new[] { "UnidadMedidaId" });
            DropIndex("dbo.Productos", new[] { "Codigo" });
            DropIndex("dbo.Ordenes", new[] { "ClientesId" });
            DropIndex("dbo.OrdenDetalles", new[] { "ProductoId" });
            DropIndex("dbo.OrdenDetalles", new[] { "OrdenId" });
            DropIndex("dbo.ClasificacionProductos", new[] { "Codigo" });
            DropTable("dbo.Usuarios");
            DropTable("dbo.UnidadMedidas");
            DropTable("dbo.Productos");
            DropTable("dbo.Ordenes");
            DropTable("dbo.OrdenDetalles");
            DropTable("dbo.Clientes");
            DropTable("dbo.ClasificacionProductos");
        }
    }
}
