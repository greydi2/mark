﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Market.Models
{
    public class Ordenes
    {
        //[Key]
        public int Id { get; set; }
        
        [Required]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Creacion")]
        public DateTime FechaCreacion { get; set; }

        [Required]
        public int ClientesId { get; set; } // Llave foranea

        [ForeignKey("ClientesId")]
        public Clientes Clientes { get; set; } // Propiedad de Navegacion

        public ICollection<OrdenDetalles> OrdenDetalles { get; set; }

        /*
        [MaxLength(50)]
        [MaxLength(50, ErrorMessage = " Debe ser menor a 50 caracteres")]

        public int ClientesId { get; set; } // Llave foranea
        public Clientes ClientesId { get; set; } // Propiedad de Navegacion
        */
    }
}