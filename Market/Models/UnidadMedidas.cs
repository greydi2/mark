﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Market.Models
{
    public class UnidadMedidas
    {
        //[Key]
        public int Id { get; set; }

        [Index(IsUnique = true)]
        [StringLength(15)]
        [Display(Name = "Codigo de Unidad")]
        public string Codigo { get; set; }

        [Required]
        [MaxLength(255)]
        public string Descripcion { get; set; }

        [Required]
        public bool Estado { get; set; }
    }
}