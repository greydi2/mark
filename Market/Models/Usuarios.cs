﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Market.Models
{
    public class Usuarios
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Usuario")]
        [MaxLength(50, ErrorMessage = " Debe ser menor a 50 caracteres")]
        public string Usuario { get; set; }

        [Required]
        [Display(Name = "Clave")]
        [MinLength(5, ErrorMessage = " Debe ser mayor a 5 caracteres")]
        public string Clave { get; set; }

        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        public bool Estado { get; set; }
    }
}