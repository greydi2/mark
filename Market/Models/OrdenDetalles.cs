﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Market.Models
{
    public class OrdenDetalles
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int OrdenId { get; set; } // Llave foranea

        [Display(Name = "Producto")]
        [Required]
        public int ProductoId { get; set; } // Llave foranea

        [Required]
        public double Precio { get; set; }
        public int Cantidad { get; set; }
        public int Descuento { get; set; }
        public double Total { get; set; }

        [ForeignKey("OrdenId")]
        public Ordenes Orden { get; set; } // Propiedad de Navegacion

        [ForeignKey("ProductoId")]
        public Productos Producto { get; set; } // Propiedad de Navegacion
    }
}