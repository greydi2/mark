﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Market.Models
{
    public class ClasificacionProductos
    {
        //[Key]
        public int Id { get; set; }

        [Index(IsUnique = true)]
        [StringLength(15)]
        [Display(Name = "Codigo de Clasificacion")]
        public string Codigo { get; set; }

        [Required]
        [StringLength(75)]
        public string Clasificacion { get; set; }

        [Required]
        public bool Estado { get; set; }
    }
}