﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Market.Models
{
    public class Productos
    {
        //[Key]
        [Display(Name = "Producto")]
        public int Id { get; set; }

        //[Required]
        [Index(IsUnique = true)]
        [StringLength(15)]
        [Display(Name = "Codigo de Producto")]
        public string Codigo { get; set; }

        [Required]
        [StringLength(75)]
        public string Nombre { get; set; }

        [Required]
        public bool Estado { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Creacion")]
        public DateTime FechaCreacion { get; set; }

        [Display(Name = "Unidad Medida")]
        public int UnidadMedidaId { get; set; } // Llave Foranea

        [Display(Name = "Clasificacion")]
        public int ClasificacionProductoId { get; set; } // Llave Foranea

        [ForeignKey("UnidadMedidaId")]
        public UnidadMedidas UnidadMedidas { get; set; }

        [ForeignKey("ClasificacionProductoId")]
        public ClasificacionProductos ClasificacionProductos { get; set; }

        [Required]
        public double Precio { get; set; }
    }
}