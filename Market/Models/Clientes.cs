﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Market.Models
{
    public class Clientes
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        //[MinLength(50, ErrorMessage = " Debe ser mas de 50 caracteres")]
        [Required]
        public string Nombre { get; set; }
    }
}