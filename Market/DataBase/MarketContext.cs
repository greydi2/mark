﻿using Market.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Market.DataBase
{
    public class MarketContext : DbContext
    {
        public MarketContext() : base("Market")
        {
        }

        public DbSet<Ordenes> TOrdenes { get; set; }
        public DbSet<Clientes> TClientes { get; set; }
        public DbSet<OrdenDetalles> TOrdenDetalles { get; set; }
        public DbSet<Productos> TProductos { get; set; }
        public DbSet<ClasificacionProductos> TClasificacionProductos { get; set; }
        public DbSet<UnidadMedidas> TUnidadMedidas { get; set; }
        public DbSet<Usuarios> TUsuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }
    }
}