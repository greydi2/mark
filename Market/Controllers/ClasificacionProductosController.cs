﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Market.DataBase;
using Market.Models;

namespace Market.Controllers
{
    public class ClasificacionProductosController : Controller
    {
        private MarketContext db = new MarketContext();

        // GET: ClasificacionProductos
        public ActionResult Index()
        {
            return View(db.TClasificacionProductos.ToList());
        }

        // GET: ClasificacionProductos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClasificacionProductos clasificacionProductos = db.TClasificacionProductos.Find(id);
            if (clasificacionProductos == null)
            {
                return HttpNotFound();
            }
            return View(clasificacionProductos);
        }

        // GET: ClasificacionProductos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ClasificacionProductos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Codigo,Clasificacion,Estado")] ClasificacionProductos clasificacionProductos)
        {
            if (ModelState.IsValid)
            {
                db.TClasificacionProductos.Add(clasificacionProductos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(clasificacionProductos);
        }

        // GET: ClasificacionProductos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClasificacionProductos clasificacionProductos = db.TClasificacionProductos.Find(id);
            if (clasificacionProductos == null)
            {
                return HttpNotFound();
            }
            return View(clasificacionProductos);
        }

        // POST: ClasificacionProductos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Codigo,Clasificacion,Estado")] ClasificacionProductos clasificacionProductos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clasificacionProductos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(clasificacionProductos);
        }

        // GET: ClasificacionProductos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClasificacionProductos clasificacionProductos = db.TClasificacionProductos.Find(id);
            if (clasificacionProductos == null)
            {
                return HttpNotFound();
            }
            return View(clasificacionProductos);
        }

        // POST: ClasificacionProductos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ClasificacionProductos clasificacionProductos = db.TClasificacionProductos.Find(id);
            db.TClasificacionProductos.Remove(clasificacionProductos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
