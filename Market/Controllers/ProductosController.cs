﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Market.DataBase;
using Market.Models;

namespace Market.Controllers
{
    public class ProductosController : Controller
    {
        private MarketContext db = new MarketContext();

        // GET: Productos
        public ActionResult Index()
        {
            var tProductos = db.TProductos.Include(p => p.ClasificacionProductos).Include(p => p.UnidadMedidas);
            return View(tProductos.ToList());
        }

        // GET: Productos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos productos = db.TProductos.Find(id);
            if (productos == null)
            {
                return HttpNotFound();
            }
            return View(productos);
        }

        // GET: Productos/Create
        public ActionResult Create()
        {
            ViewBag.ClasificacionProductoId = new SelectList(db.TClasificacionProductos, "Id", "Codigo");
            ViewBag.UnidadMedidaId = new SelectList(db.TUnidadMedidas, "Id", "Codigo");
            return View();
        }

        // POST: Productos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Codigo,Nombre,Estado,FechaCreacion,UnidadMedidaId,ClasificacionProductoId,Precio")] Productos productos)
        {
            if (ModelState.IsValid)
            {
                db.TProductos.Add(productos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClasificacionProductoId = new SelectList(db.TClasificacionProductos, "Id", "Codigo", productos.ClasificacionProductoId);
            ViewBag.UnidadMedidaId = new SelectList(db.TUnidadMedidas, "Id", "Codigo", productos.UnidadMedidaId);
            return View(productos);
        }

        // GET: Productos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos productos = db.TProductos.Find(id);
            if (productos == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClasificacionProductoId = new SelectList(db.TClasificacionProductos, "Id", "Codigo", productos.ClasificacionProductoId);
            ViewBag.UnidadMedidaId = new SelectList(db.TUnidadMedidas, "Id", "Codigo", productos.UnidadMedidaId);
            return View(productos);
        }

        // POST: Productos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Codigo,Nombre,Estado,FechaCreacion,UnidadMedidaId,ClasificacionProductoId,Precio")] Productos productos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClasificacionProductoId = new SelectList(db.TClasificacionProductos, "Id", "Codigo", productos.ClasificacionProductoId);
            ViewBag.UnidadMedidaId = new SelectList(db.TUnidadMedidas, "Id", "Codigo", productos.UnidadMedidaId);
            return View(productos);
        }

        // GET: Productos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos productos = db.TProductos.Find(id);
            if (productos == null)
            {
                return HttpNotFound();
            }
            return View(productos);
        }

        // POST: Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Productos productos = db.TProductos.Find(id);
            db.TProductos.Remove(productos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
