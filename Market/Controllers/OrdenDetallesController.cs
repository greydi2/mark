﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Market.DataBase;
using Market.Models;

namespace Market.Controllers
{
    public class OrdenDetallesController : Controller
    {
        private MarketContext db = new MarketContext();

        // GET: OrdenDetalles
        public ActionResult Index()
        {
            var tOrdenDetalles = db.TOrdenDetalles.Include(o => o.Orden).Include(o => o.Producto);
            return View(tOrdenDetalles.ToList());
        }

        // GET: OrdenDetalles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrdenDetalles ordenDetalles = db.TOrdenDetalles.Find(id);
            if (ordenDetalles == null)
            {
                return HttpNotFound();
            }
            return View(ordenDetalles);
        }

        // GET: OrdenDetalles/Create
        public ActionResult Create()
        {
            ViewBag.OrdenId = new SelectList(db.TOrdenes, "Id", "Id");
            ViewBag.ProductoId = new SelectList(db.TProductos, "Id", "Codigo");
            return View();
        }

        // POST: OrdenDetalles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OrdenId,ProductoId,Precio,Cantidad,Descuento,Total")] OrdenDetalles ordenDetalles)
        {
            if (ModelState.IsValid)
            {
                db.TOrdenDetalles.Add(ordenDetalles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrdenId = new SelectList(db.TOrdenes, "Id", "Id", ordenDetalles.OrdenId);
            ViewBag.ProductoId = new SelectList(db.TProductos, "Id", "Codigo", ordenDetalles.ProductoId);
            return View(ordenDetalles);
        }

        // GET: OrdenDetalles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrdenDetalles ordenDetalles = db.TOrdenDetalles.Find(id);
            if (ordenDetalles == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrdenId = new SelectList(db.TOrdenes, "Id", "Id", ordenDetalles.OrdenId);
            ViewBag.ProductoId = new SelectList(db.TProductos, "Id", "Codigo", ordenDetalles.ProductoId);
            return View(ordenDetalles);
        }

        // POST: OrdenDetalles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OrdenId,ProductoId,Precio,Cantidad,Descuento,Total")] OrdenDetalles ordenDetalles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ordenDetalles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrdenId = new SelectList(db.TOrdenes, "Id", "Id", ordenDetalles.OrdenId);
            ViewBag.ProductoId = new SelectList(db.TProductos, "Id", "Codigo", ordenDetalles.ProductoId);
            return View(ordenDetalles);
        }

        // GET: OrdenDetalles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrdenDetalles ordenDetalles = db.TOrdenDetalles.Find(id);
            if (ordenDetalles == null)
            {
                return HttpNotFound();
            }
            return View(ordenDetalles);
        }

        // POST: OrdenDetalles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrdenDetalles ordenDetalles = db.TOrdenDetalles.Find(id);
            db.TOrdenDetalles.Remove(ordenDetalles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
